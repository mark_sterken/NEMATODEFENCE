################################################################################
### R-script for NEMATODEFENCE training day ####################################
### Mark Sterken, 29/5/2017 ####################################################
################################################################################


################################################################################
###set wd, load packages and additional functions

    ###Set your work directory
    setwd("D:/Trainings_data/R/")
    workwd <- getwd()
    filename <- "NEMATODEFENCE"

    ###Load functions for mapping
    source("D:/Trainings_data/Functions/NEMATODEFENCE_QTL.R")

    ###Packages
    install <- FALSE
    if(install){
        install.packages("tidyverse")
        install.packages("openxlsx")
        install.packages("gridExtra")
    }
    library(tidyverse)
    library(openxlsx)
    library(gridExtra)

    presentation <- theme(axis.text.x = element_text(size=16, face="bold", color="black"),
                          axis.text.y = element_text(size=16, face="bold", color="black"),
                          axis.title.x = element_text(size=20, face="bold", color="black"),
                          axis.title.y = element_text(size=20, face="bold", color="black"),
                          strip.text.x = element_text(size=20, face="bold", color="black"),
                          strip.text.y = element_text(size=20, face="bold", color="black"),
                          plot.title = element_text(size=24, face="bold"),
                          panel.grid.minor = element_blank(),
                          panel.grid.major = element_blank(),
                          legend.position = "right")

    blank_theme <- theme(plot.background = element_blank(),
                         panel.grid.major = element_blank(),
                         panel.grid.minor = element_blank(),
                         panel.border = element_blank(),
                         panel.background = element_blank(),
                         axis.title.x = element_blank(),
                         axis.title.y = element_blank(),
                         axis.text.x = element_blank(),
                         axis.text.y = element_blank(),
                         axis.ticks = element_blank())


################################################################################
###Load the data

    ###The data loading is handled by additional scripts that are optimized for each
    ###plate (as there are some exceptions to handle per plate)
    setwd("./Data")
        source("Master_loader.R")
        head(pheno)
        head(cluster)
        head(allplates)
    setwd(workwd)




################################################################################
###Clean the data

    ###clean data
        ###Filter multiple reads per marker per genotype per plate per well (because extra reads don't indicate better quality)
        ###Filter all markers with less than 6 detections (arbitrary)
        ###Exclude genotypes per plate per well if they have <100 reads
        filter_markers <- 5
        filter_reads <- 99

        data.clean <- group_by(allplates,plate,well,genotype) %>%
                      filter(!duplicated(marker)) %>%
                      group_by(marker) %>%
                      mutate(n=length(plate)) %>%
                      filter(n>filter_markers) %>%
                      group_by(plate,well,genotype) %>%
                      mutate(n=length(plate)) %>%
                      filter(n>filter_reads) %>%
                      data.frame()


    ###Explore the data
        original <- TRUE
        if(!original){data.check <- data.clean}
        if(original){data.check <- allplates}

        ###Check the reads per primer combination
            plot.data <- data.check

            p1 <- ggplot(plot.data,aes(x=primer_cluster)) +
                  geom_bar(stat="count") + facet_grid(plate~.) +
                  theme(legend.position = "none",axis.text.x = element_text(angle = 45, hjust = 1)) +
                  labs(x="Primer combination",y="Reads (n)",title="Reads per primer combination")

        ###The reads as grouped per 'marker'
            plot.data <- group_by(data.check,marker) %>%
                         filter(!duplicated(paste(well,genotype))) %>%
                         summarize(n=length(plate)) %>%
                         data.frame() %>%
                         arrange(n) %>%
                         mutate(marker=factor(marker,levels=unique(marker))) %>%
                         mutate(marker=as.numeric(marker))

            p2 <- ggplot(plot.data,aes(x=marker,y=n)) +
                  geom_bar(stat="identity") +
                  theme(legend.position = "none") + scale_y_log10() +
                  labs(x="Alleles (markers)",y="Unique genotypes (n)",title="Unique genotypes per marker")

        ###The number of reads per well
            plot.data <- group_by(data.check,plate,well,genotype) %>%
                         filter(!duplicated(marker)) %>%
                         summarize(n=length(plate)) %>%
                         data.frame() %>%
                         arrange(n) %>%
                         mutate(name=paste(genotype,plate,well,sep=":")) %>%
                         mutate(name=factor(name,levels=unique(name)))

            p3 <- ggplot(plot.data,aes(x=name,y=n,fill=plate)) +
                  geom_bar(stat="identity") +
                  theme(legend.position = "none",axis.text.x = element_text(angle = 45, hjust = 1)) +
                  labs(x="Genotypes (per well)",y="unique markers (n)",title="Unique markers per well")

        ###The correlation between repeated genotypes
            plot.data <- group_by(data.check,plate,well,genotype) %>%
                         filter(!duplicated(marker)) %>%
                         summarize(n=length(plate)) %>%
                         data.frame() %>%
                         filter(genotype != "EMPTY") %>%
                         group_by(genotype) %>%
                         mutate(n_geno=length(genotype),n_mean=mean(n)) %>%
                         filter(n_geno>1) %>%
                         data.frame() %>%
                         mutate(R2=round(R.sq(n,n_mean),digits=2))

            p4 <- ggplot(plot.data,aes(x=n_mean,y=n)) +
                  geom_point() + geom_smooth(method="lm",colour="black") + geom_text(aes(x=150,y=250,label=paste("R^2=",R2,sep="")),size=6) +
                  theme(legend.position = "none") + labs(x="Mean unique markers\nper genotype",y="unique markers (n)",title="Replicated genotypes")

        ###The overlap between repeated genotypes in markers
        ###Make sure that the markers are comparable: either plate set 9,10,11 or within same plate
            plot.data <- group_by(data.check,genotype) %>%
                         filter(genotype != "EMPTY") %>%
                         mutate(n_geno=length(unique(paste(plate,well)))) %>%
                         filter(n_geno>1) %>%
                         mutate(test=(sum(unique(plate) %in% c(9, 10, 11)) == length(unique(plate))) |
                                     (sum(unique(plate) %in% c(1)) == length(unique(plate))) |
                                     (sum(unique(plate) %in% c(2)) == length(unique(plate))) |
                                     (sum(unique(plate) %in% c(3)) == length(unique(plate)))) %>%
                         filter(test) %>%
                         group_by(genotype,well,plate,marker) %>%
                         mutate(n_marker=length(marker)) %>%
                         data.frame() %>%
                         group_by(genotype,marker) %>%
                         mutate(overlap_all=length(unique(paste(plate,well))),overlap_multi=length(unique(paste(plate,well)[n_marker>1]))) %>%
                         data.frame() %>%
                         filter(!duplicated(paste(marker,genotype))) %>%
                         select(population:n_geno,overlap_all,overlap_multi) %>%
                         mutate(overlap_all=overlap_all/n_geno,overlap_multi=overlap_multi/n_geno) %>%
                         gather(key=Selection,value=fraction,-c(population:n_geno)) %>%
                         mutate(fraction=round(fraction,digits=2))

            p5 <- ggplot(plot.data,aes(x=Selection,fill=as.factor(fraction))) +
                  geom_bar() + labs(x="Selection",y="Counts (fraction overlap)",title="Overlap in markers\nper replicate")


        ###Some layout things
            BlPl <- ggplot()+geom_blank(aes(1,1))+ blank_theme
            lay <- rbind(c(1,1,2,3,4),rep(5,5))

            if(original){pdf(file="Plate_overview_all.pdf",width=28,height=12)}
            if(!original){pdf(file="Plate_overview_clean.pdf",width=28,height=12)}
                grid.arrange(p1,p2,p4,p5,p3,layout_matrix=lay)
            dev.off()



    ###Make overview
    summary_table <- group_by(allplates,plate,well,genotype) %>%
                     filter(!duplicated(marker)) %>%
                     mutate(n_unique_markers_all=length(marker),n_unique_primers_all=length(unique(primer_cluster)),primers_all=paste(unique(primer_cluster),collapse=", ")) %>%
                     mutate(tmp=ifelse(plate %in% c(9,10,11),99,plate)) %>%
                     group_by(genotype,tmp) %>%
                     mutate(n_replicates_all=length(unique(paste(plate,well))),n2=length(unique(marker))) %>%
                     group_by(genotype,tmp,marker) %>%
                     mutate(n1=length(marker)) %>%
                     group_by(genotype,tmp,plate,well) %>%
                     mutate(fraction_overlap_replicates_all=sum(n1>1)/n2) %>%
                     data.frame() %>%
                     group_by(marker) %>%
                     mutate(n=length(plate)) %>%
                     filter(n>filter_markers) %>%
                     group_by(plate,well,genotype) %>%
                     mutate(n=length(plate)) %>%
                     data.frame() %>%
                     group_by(plate,well,genotype) %>%
                     mutate(n_unique_markers_clean=length(marker[n>filter_reads]),n_unique_primers_clean=length(unique(primer_cluster[n>filter_reads])),primers_clean=paste(unique(primer_cluster[n>filter_reads]),collapse=", ")) %>%
                     mutate(tmp=ifelse(plate %in% c(9,10,11),99,plate)) %>%
                     group_by(genotype,tmp) %>%
                     mutate(n_replicates_clean=length(unique(paste(plate,well))),n2=length(unique(marker))) %>%
                     group_by(genotype,tmp,marker) %>%
                     mutate(n1=length(marker)) %>%
                     group_by(genotype,tmp,plate,well) %>%
                     mutate(fraction_overlap_replicates_clean=sum(n1>1)/n2) %>%
                     data.frame() %>%
                     filter(!duplicated(paste(plate,well))) %>%
                     select(-tmp,-n1,-n2,-n) %>%
                     select(plate,well,population:fraction_overlap_replicates_clean) %>%
                     arrange(plate,well)

    write.table(summary_table,file="Sequencing_summary.txt",quote=F,sep="\t")
    write.xlsx(summary_table,file="Sequencing_summary.xlsx")



    save(data.clean,file="data.clean.Rdata")


################################################################################
###Plate 9,10,11: variety panel

    #load(data.clean)

    data.use <- filter(data.clean,population%in%c("variety.panel"))

    ###Look at the number of reads
        data.plot <- group_by(data.use,population) %>%
                     mutate(genotype_n=length(unique(genotype))) %>%
                     group_by(population,marker) %>%
                     mutate(marker_n=length(unique(genotype))) %>%
                     data.frame() %>%
                     filter(!duplicated(paste(population,marker))) %>%
                     mutate(fraction=marker_n/genotype_n) %>%
                     group_by(population) %>%
                     arrange(fraction) %>%
                     mutate(marker=as.numeric(factor(marker,levels=unique(marker))))

        p1 <- ggplot(data.plot,aes(x=marker,y=fraction)) +
              geom_bar(stat="identity") + facet_grid(.~population,scales="free_x") +
              geom_hline(yintercept=c(0.05,0.95),colour="red") + labs(x="Marker (number)",y="Fraction of population")

    ###Prepare the map files
        data.plot <- mutate(data.use,genotype_n=length(unique(genotype))) %>%
                     group_by(genotype) %>%
                     filter(!duplicated(marker)) %>%
                     group_by(marker) %>%
                     mutate(marker_n=length(marker)) %>%
                     filter(marker_n/genotype_n > 0.05 & marker_n/genotype_n < 0.95) %>%
                     data.frame() %>%
                     select(primer_cluster,marker,NB_LRR_cluster,genotype) %>%
                     mutate(present=1) %>%
                     spread(key=genotype,value=present,fill=-1)

         map.variety.panel <- data.matrix(data.plot[,-c(1:3)])
             rownames(map.variety.panel) <- data.plot$NB_LRR_cluster

         mrk.variety.panel <-  separate(data.plot,NB_LRR_cluster,into=c("bla","Chromosome","Position"),sep="-") %>%
                       rename(name=marker) %>%
                       group_by(Chromosome,Position) %>%
                       mutate(tmp=c(0:(length(Position)-1))) %>%
                       data.frame() %>%
                       mutate(Position=ifelse(tmp <10,paste(as.character(unlist(Position)),".0",tmp,sep=""),paste(as.character(unlist(Position)),tmp,sep="."))) %>%
                       mutate(Position=as.numeric(Position)) %>%
                       select(name,Chromosome,Position)

         heatmap(cor(t(map.variety.panel))^2,scale="none")

         write.xlsx(map.variety.panel,file = "map.variety.panel.xlsx")
         write.xlsx(mrk.variety.panel,file = "marker.variety.panel.xlsx")

     ###Plot the map
         data.plot <- cbind(mrk.variety.panel,map.variety.panel) %>%
                      gather(key=strain,value=genotype,-name,-Chromosome,-Position)


         p2 <- ggplot(data.plot, aes(x=as.factor(Position),y=strain,fill=genotype,alpha=0.2)) +
               geom_raster() + facet_grid(.~Chromosome,scales="free_x",space="free_x") +
               scale_x_discrete(breaks=c(1:10),labels=c(1:10)) +
               theme(legend.position = "none") + labs(x="Position (marker)",y="Strain")


     ###Map the phenotype
         pheno.variety.panel.tmp <- filter(pheno,population=="variety.panel") %>%
                                    select(plate:ploidy,RV_Ro1:resE_Pa3) %>%
                                    gather(key=trait,value=value,-c(plate:ploidy)) %>%
                                    mutate(value=ifelse(toupper(value)=="R",1,ifelse(toupper(value)=="S",0,value))) %>%
                                    spread(key=trait,value=value) %>%
                                    select(genotype,res_Pa2:RV_Ro2_3) %>%
                                    filter(genotype %in% colnames(map.variety.panel))
         
         
         plot.data <- gather(pheno.variety.panel.tmp,key=trait,value=value,-genotype) %>%
                      mutate(value=as.numeric(as.character(unlist(value))))
           
         
         p3 <- ggplot(plot.data,aes(x=value)) +
               geom_histogram(binwidth=1) + facet_grid(~trait,scales="free_x")
         
         
         pheno.variety.panel <- data.matrix(t(pheno.variety.panel.tmp[,-1]))
             colnames(pheno.variety.panel) <- pheno.variety.panel.tmp[,1]

         data.variety.panel <- QTL.data.prep(pheno.variety.panel,colnames(pheno.variety.panel),map.variety.panel,mrk.variety.panel); lapply(data.variety.panel,head)


         variety.panel.QTL <- map.1(data.variety.panel[[1]],data.variety.panel[[2]],data.variety.panel[[3]],NAs=TRUE)

         variety.panel.peak <- map1.to.dataframe(map1.output=variety.panel.QTL)


         p4 <- ggplot(variety.panel.peak, aes(x=as.factor(qtl_bp),y=qtl_significance,alpha=0.2,colour=qtl_significance>3)) +
               geom_point() + facet_grid(trait~qtl_chromosome,scales="free_x",space="free_x") +
               scale_x_discrete(breaks=c(1:10),labels=c(1:10)) +
               theme(legend.position = "none") + labs(x="Position (marker)",y="Significance (-log10(p))")

         tmp <- group_by(variety.panel.peak,trait) %>%
                mutate(qtl_FDR=p.adjust(10^-qtl_significance,method="fdr")) %>%
                filter(qtl_significance>3) %>%
                merge(mrk.variety.panel,by.x=c(2,3),by.y=c(2,3)) %>%
                rename(qtl_trinity_marker=name)

         write.xlsx(tmp,file="variety.panel_peaks.xlsx")


         ############################################################################
         ###Blank placeholder
         BlPl <- ggplot()+geom_blank(aes(1,1))+ blank_theme


         pdf("variety.panel.pdf",width=30,height=12)
             print(grid.arrange(p1,BlPl,ncol=2))
             print(p2)
             print(p3)
             print(p4)
         dev.off()








