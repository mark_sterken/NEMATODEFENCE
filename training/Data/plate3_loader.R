################################################################################
###Plate 3 loader

location <- "plate_3"
files <- dir(location)
    files <- files[!grepl(".xlsx",files)]
    files <- files[!grepl("clusters_excel",files)]
    files <- files[!grepl(".out",files)]
    files <- paste(location,"/",files,sep="")

out <- NULL
for(i in 1:length(files)){

    tmp1 <- read.table(files[i],header=F,sep="\n")
        tmp1 <- lapply(apply(tmp1,1,strsplit,split=" "),organizer)

    tmp2 <- NULL
    for(j in 1:length(tmp1)){
        tmp2 <- rbind(tmp2,tmp1[[j]])
    }

    tmp2 <- cbind(primer=unlist(strsplit(unlist(strsplit(files[i],split="_primer_"))[2],split="_sorted_"))[1],tmp2) %>%
            mutate(marker=gsub("marker_","",marker),identifier.tmp=1:length(primer),plate=3) %>%
            separate(dat,into=c("location","sample","cluster","cluster_number","trinity"),sep="_") %>%
            group_by(identifier.tmp) %>%
            filter(checker(location,what="-")==1) %>%
            separate(location,into=c("row","number"),sep="-") %>%
            separate(sample,into=c("bla1","bla2","primer_1"),sep="\\.") %>%
            separate(cluster,into=c("primer_2","bla3"),sep="\\.") %>%
            mutate(number=as.character(number),trinity=gsub("trinity.Trinity.","",trinity)) %>%
            mutate(well=ifelse(!number %in% as.character(10:12),paste(row,"0",number,sep=""),paste(row,number,sep="")),
                   primer_cluster=ifelse(primer_1==primer_2,primer_1,paste(primer_1,primer_2,sep="-"))) %>%
            data.frame() %>%
            select(primer,primer_cluster,marker,plate,well,cluster_number,trinity) %>%
            mutate(marker=paste("3",primer_cluster,marker,sep="_"),plate=as.numeric(as.character(unlist(plate))))

    out <- rbind(out,tmp2)
}

plate3 <- out
save(plate3,file=paste(location,"/","obj_plate3.out",sep=""))