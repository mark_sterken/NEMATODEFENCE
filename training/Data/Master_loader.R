################################################################################
###Master loader

###function
organizer <- function(x){x<-unlist(x);data.frame(cbind(marker=x[1],dat=x[-1]))}
checker <- function(x,what){length(gregexpr(what,x)[[1]])}


run.loader <- FALSE

###Load plate 1
if(run.loader){source("plate1_loader.R")}
if(!run.loader){load("plate_1diy/obj_plate1.out")}

###Load plate 2
if(run.loader){source("plate2_loader.R")}
if(!run.loader){load("plate_2/obj_plate2.out")}

###Load plate 3
if(run.loader){source("plate3_loader.R")}
if(!run.loader){load("plate_3/obj_plate3.out")}

###Load plate 9, 10, 11
if(run.loader){source("plate9.10.11_loader.R")}
if(!run.loader){load("plate_9+10+11/obj_plate91011.out")}

################################################################################
###Combine, add genotypes and cluster info

    ###Load genotypes
    pheno <- read.xlsx(xlsxFile="phenotype_data_R.xlsx",sheet=1)
        geno <- pheno[,c(1,2,3,4,5,6)]

    ###Load cluster info
    cluster <- read.xlsx(xlsxFile="primer-cluster_list.xlsx",sheet=1)
        colnames(cluster) <- c("primer_cluster","NB_LRR_cluster")

    ###Combine
    allplates <- rbind(plate1,plate2,plate3,plate91011) %>%
                 merge(geno) %>%
                 merge(cluster) %>%
                 mutate(primer_cluster=factor(primer_cluster,levels=unique(cluster$primer_cluster)))


################################################################################
###END





